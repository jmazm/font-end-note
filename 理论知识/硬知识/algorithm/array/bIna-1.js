/**
 * 题目
    我们给出两个单词数组 A 和 B。每个单词都是一串小写字母。
    现在，如果 b 中的每个字母都出现在 a 中，包括重复出现的字母，那么称单词 b 是单词 a 的子集。 例如，“wrr” 是 “warrior” 的子集，但不是 “world” 的子集。
    如果对 B 中的每一个单词 b，b 都是 a 的子集，那么我们称 A 中的单词 a 是通用的。
    你可以按任意顺序以列表形式返回 A 中所有的通用单词。
 */



function calcBTotal(B) {
    let tmp = {}
    let total = {}

    // 先统计B中每项中每个字母的出现次数
    B.forEach((item, index) => {
        tmp[index] = {}
        item.split('').forEach((b) => {
            if (!tmp[index][b]) {
                tmp[index][b] = 1
            } else {
                tmp[index][b]++
            }
        })
    })

    // 再统计每个字母出现的最高次数
    Object.values(tmp).forEach((item) => {
        for (let b in item) {
            if (!total[b]) {
                total[b] = item[b]
            } else {
                if (item[b] > total[b]) {
                    total[b] = item[b]
                }
            }
        }
    })

    return total
}


// 统计b中每个字符在a中出现的次数
function calcTotalBInA(a, b) {
    const reg = new RegExp(b, 'g')

    return (a.match(reg) || []).length
}


// 判断A中的单词是否是通用单词
function judgeAWords(obja, objb) {
    for (let b in objb) {
        // 只要a中的任一个属性值比b中对应的属性的值小，就返回false
        if (obja[b] < objb[b]) {
            return false
        }
    }

    return true
}

/**
 * wordSubsets
 * @param {string[]} A
 * @param {string[]} B
 * @return {string[]}
 */
export default (A, B) => {
    const total = calcBTotal(B)

    const letters = Object.keys(total)

    const tmp = {}

    const result = []

    for (let l of letters) {
        for (let a of A) {
            if (!tmp[a]) {
                tmp[a] = {}
            }
            // 计算b中每个字母在a中的次数
            tmp[a][l] = calcTotalBInA(a, l)
        }
    }


    Object.values(tmp).forEach((item, index) => {
        if (judgeAWords(item, total)) {
            result.push(Object.keys(tmp)[index])
        }
    })

    return result
}