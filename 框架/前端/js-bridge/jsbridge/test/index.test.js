var chai = require('chai');
var spies = require('chai-spies');
chai.use(spies);
var {should, spy} = chai;
should();

global.window = {};

var JsBridge = require('../dist/index');
var jsbridge = new JsBridge('test');
var cb = 'jsbridge_cb_0_';

describe('url scheme', () => {
    it('only command', () => {
        jsbridge.getUrl('command').should.equal('test://command');
    });

    it('params', () => {
        jsbridge.getUrl('command', {a: 1, b: 2}).should.equal('test://command?a=1&b=2');
    });

    it('url encode', () => {
        const URL = 'http://163.com';
        jsbridge.getUrl('command', {url: URL}).should.equal(`test://command?url=${encodeURIComponent(URL)}`);
    });

    it('callback1', done => {
        let callback = spy(() => {});
        let id = 0;
        jsbridge.getUrl('command', {callback}).should.equal(`test://command?callback=${cb}${id}`);
        jsbridge.call(cb + id);

        setTimeout(() => {
            jsbridge.call(cb + id);
            callback.should.be.called(1);
            done();
        }, 100);
    });

    it('callback2', done => {
        let callback = spy(() => true);
        let id = 1;
        jsbridge.getUrl('command', {callback}).should.equal(`test://command?callback=${cb}${id}`);
        jsbridge.call(cb + id, 'a', 'b');

        setTimeout(() => {
            jsbridge.call(cb + id);
            callback.should.be.called(2);
            callback.should.be.called.with('a', 'b');
            done();
        }, 100);
    });
});