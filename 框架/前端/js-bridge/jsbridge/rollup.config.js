import babel from 'rollup-plugin-babel';
import {uglify} from 'rollup-plugin-uglify';

export default {
    input: './src/index.js',
    output: {
        name: 'JsBridge',
        file: './dist/index.js',
        format: 'umd',
    },
    plugins: [
        babel(),
        uglify(),
    ]
};