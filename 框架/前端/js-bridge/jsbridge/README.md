# JsBridge

## 安装

```shell
npm i @fe/jsbridge
```

## 使用

```js
import JsBridge from '@fe/jsbridge';
var jsBridge = new JsBridge('xxxbridge');

jsBridge.invoke('test'); // xxxbridge://test
```