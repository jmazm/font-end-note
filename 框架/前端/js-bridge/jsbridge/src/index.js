let callbackCounter = 0;
let invokeQueue = [];
let invokeTimestamp = 0;
let jsBridgeCount = window.jsBridgeCount || 0;

const INVOKE_INTERVAL = 100;
const CALLBACK_PREFIX = `jsbridge_cb_${jsBridgeCount}_`;
window.jsBridgeCount += 1;

class JsBridge {
    constructor (protocol) {
        this.protocol = protocol.replace(/:\/\/$/, '');

        console.log(this.protocol)
    }

    /**
     * 调用 Native
     * @param {*} api - bridgeName
     * @param {*} params 
     */
    invoke (api, params = {}) {
        let now = Date.now();
        let {protocol} = this;

        // 这里用时间戳是因为在安卓机上，如果连续发scheme，有可能无法发送成功
        if (now - invokeTimestamp >= INVOKE_INTERVAL) {
            invokeTimestamp = now;

            parse(protocol, api, params);

            checkQueue();
        } else {
            invokeQueue.push([protocol, api, params]);
        }
        return this;
    }

    call (cb) {
        let callback = window[cb];
        let args = Array.from(arguments);
        args.shift();
        callback && callback(...args);
    }

    getUrl (api, params) {
        return toUrlScheme(this.protocol, api, params);
    }

    setProtocol (proto) {
        this.protocol = proto;
    }
}

function checkQueue () {
    setTimeout(() => {
        if (invokeQueue.length) {
            parse(...invokeQueue.shift());
            checkQueue();
        }
    }, INVOKE_INTERVAL);
}

function parse (proto, api, params) {
    exec(proto, toUrlScheme(proto, api, params));
}

function exec (proto, urlScheme) {
    if (urlScheme) {
        if (window.webkit && window.webkit.messageHandlers) {
            window.webkit.messageHandlers[proto].postMessage(urlScheme);
        } else {
            var iframe = document.createElement('iframe');
            var dom = document.documentElement;

            iframe.style.cssText = 'width:1px;height:1px;display:none;';
            iframe.src = urlScheme;
            dom.appendChild(iframe);

            setTimeout(() => {
                dom.removeChild(iframe);
            }, 100);
        }
    }
}

function toUrlScheme (proto, api, params) {
    let urlScheme = '';
    let paramsList = [];
    for (let key in params) {
        let value = params[key];
        console.log(value, params)

        if (typeof value === 'object') {
            value = JSON.stringify(value);
        } else if (typeof value === 'function') {
            value = toCallbackId(value);
        }

        value = encodeURIComponent(value);

        paramsList.push(key + '=' + value);
    }
    urlScheme = proto + '://' + api + (paramsList.length > 0 ? '?' + paramsList.join('&') : '');
    return urlScheme;
}

function toCallbackId (callback) {
    let funcName = CALLBACK_PREFIX + callbackCounter++;

    window[funcName] = function jsBridgeCallback () {
        if (callback(...arguments) !== true) {
            callback = null;
            window[funcName] = null;
            delete window[funcName];
        }
    };

    return funcName;
}

// export default JsBridge;
// module.exports = JsBridge;